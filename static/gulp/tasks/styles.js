var gulp = require('gulp');
var sass = require('gulp-sass');
var sassGlob = require('gulp-sass-glob');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');

var browserSync = require('./browser-sync');
var combiner = require('../common');

gulp.task('styles', function () {
  return combiner.getCombinerPipe('styles', [
    gulp.src([
      'src/styles/main.scss'
    ]),
    sourcemaps.init(),
    sassGlob(),
    sass(),
    sourcemaps.write(),
    gulp.dest('.tmp/styles'),
    browserSync.stream()
  ]);
});
