var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');

var combiner = require('../common');

gulp.task('scripts', function () {
  return combiner.getCombinerPipe('scripts', [
    gulp.src([
      'src/scripts/**/*.js'
    ]),
    sourcemaps.init(),
    concat('app.js'),
    sourcemaps.write(),
    gulp.dest('.tmp/scripts')
  ]);
});
