var gulp = require('gulp');
var watch = require('gulp-watch');

var browserSync = require('./browser-sync');

gulp.task('watch', function () {
  watch([
    'src/styles/**/*.scss'
  ], function () {
    gulp.start('styles');
  });

  watch([
    'src/scripts/*.js'
  ], function () {
    gulp.start('scripts');
  });

  watch([
    'src/*.html'
  ], browserSync.reload);
});
