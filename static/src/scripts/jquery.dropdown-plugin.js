$.fn.dropdownHandler = function () {
  return this.each(function() {
    var $html = $('html');
    var $dropdown = $(this);
    var $trigger = $dropdown.find('.dropdown-trigger');
    var $content = $dropdown.find('.dropdown__content');

    $trigger.bind('click', function (event) {
      $content.toggleClass('dropdown__content--open');
      event.stopPropagation();

      $html.bind('click', function () {
        $content.removeClass('dropdown__content--open');
        $html.unbind('click');
      });
    });
  });
};

