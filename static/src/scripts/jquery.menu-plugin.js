$.fn.menuCollapse = function () {
  return this.each(function () {
    var $menu = $(this);

    $menu.on('click', function () {
      $menu.toggleClass('menu__nav--collapse--in');
    });
  });
};

$.fn.menuHandler = function () {
  return this.on('click', function () {
    $('#menu').toggleClass('menu--in');
    $('#backdrop').toggleClass('backdrop--in');
  });
};
