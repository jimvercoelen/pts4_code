var photoEditorSDK = null;
var imageUrl;

function initializePhotoEditor(photo) {
  var initImage = new Image();
  var editor = document.getElementById('photo-editor');

  initImage.addEventListener('load', function () {
    if (photoEditorSDK !== null) {
      photoEditorSDK.setImage(initImage);
      return;
    }

    photoEditorSDK = new PhotoEditorSDK.UI.ReactUI({
      container: editor,
      title: '',
      enableUpload: false,
      enableWebcam: false,
      editor: {
        image: initImage,
        export: {
          showButton: false
        }
      },
      assets: {
        baseUrl: 'assets'
      }
    });
  });

  initImage.src = photo;


}
