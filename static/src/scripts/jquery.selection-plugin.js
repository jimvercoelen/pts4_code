$.fn.selectionAnimation = function () {
  return this.each(function() {
    var $html = $('html');
    var $selection = $(this);
    var $label = $selection.find('.selection__label');
    var $icon = $selection.find('.selection__icon');
    var $control = $selection.find('.selection__control');
    var $options = $selection.find('.selection__option');

    if ($control.text() !== '') {
      $label.css('transition-duration', '0s');
      $label.css('transform', 'scale(0.85714, 0.85714) translateY(-1.35rem)');
    }

    if ($selection.hasClass('selection--disabled')) return;

    $control.on('click', handleClick);
    $icon.on('click', handleClick);

    // Handle styling on click (control or icon)
    function handleClick() {
      $selection
        .addClass('selection--focus')
        .addClass('selection--floating')
        .addClass('selection--show-options');

      // Handle styling on html click
      event.stopPropagation();
      $html.bind('click', function () {
        $html.unbind('click');
        $selection.removeClass('selection--show-options');

        if ($control.text() !== '') {
          $selection.removeClass('selection--focus');
          jQuery.event.trigger('selectionValueChanged', { selection: $selection });
        } else {
          $selection.removeClass('selection--focus');
          $selection.removeClass('selection--floating');
        }
      });
    }

    // Set value of selected option to control
    // and remove options
    $options.on('click', function () {
      var $option = $(this);
      $control.text($option.text());
      $selection.removeClass('selection--show-options');
    });

    $selection.css('visibility', 'visible');
  });
};



