$(document).ready(function () {
  var $album = $('.album');
  var $folderGrid = $('.folders');

  $album.imagesLoaded(function () {
    $album.isotope({
      itemSelector: '.image',
      layoutMode: 'masonry',
      masonry: {
        columnWidth: $album.width() / 12
      }
    });
  });

  $folderGrid.isotope({
    itemSelector: '.folder',
    layoutMode: 'masonry',
    masonry: {
      columnWidth: $album.width() / 8,
      gutter: 20
    }
  });
});
