$(document).ready(function () {
  if (window.location.href.indexOf("cart") === -1) return;

  var cart = JSON.parse(localStorage.getItem('cart'));
  var $cartProductList = $('.cart-product-list');

  displayProducts();
  displayPrice();

  /**
   * Display all products from the local storage
   * on to cart's list.
   */
  function displayProducts () {
    for (var i = 0, l = cart.products.length; i < l; i++) {
      var product = cart.products[i];
      addItemToCartList(product);
    }
  }

  function displayPrice () {
    $cartProductList.find('.cart-product-list__total').remove();
    cart = JSON.parse(localStorage.getItem('cart'));
    var totalPriceElement = document.createElement('p');
    totalPriceElement.className = 'cart-product-list__total';
    totalPriceElement.appendChild(document.createTextNode('€ ' + (cart.totalPrice).toFixed(2)));
    $cartProductList.append(totalPriceElement);
  }

  function handleRemoveClick (itemElement, product) {
    var nodeList = Array.prototype.slice.call( document.getElementById('cart-product-list').children );
    var index = nodeList.indexOf(itemElement);
    $cartProductList.find('.cart-product-list__item').eq(index).animate({ height: 'toggle' }, 'fast');
    removeFromCart(product);
    displayPrice();
  }

  /**
   * Manual adding an item (product) on cart's list.
   */
  function addItemToCartList (product) {
    var itemElement = document.createElement('div');
    itemElement.className = 'cart-product-list__item';
    itemElement.style.display = 'none';                                 // set display none so that the element can be animated

    var imgElement = document.createElement('img');
    imgElement.className = 'cart-product-list__item__img';
    imgElement.src = product.image;

    var detailsElement = document.createElement('div');
    detailsElement.className = 'cart-product-list__item__details';

    var productElement = document.createElement('p');
    productElement.appendChild(document.createTextNode(product.product));

    var optionsElement = document.createElement('p');
    var optionsText = '';
    for (var key in product) {
      if (key === 'image' || key === 'product' || key === 'price') {
        // ignore
      } else {
        optionsText += product[key] + ', ';
      }
    }
    optionsElement.appendChild(document.createTextNode(optionsText));

    detailsElement.appendChild(productElement);
    detailsElement.appendChild(optionsElement);

    var priceElement = document.createElement('p');
    priceElement.className = 'cart-product-list__item__price';
    priceElement.appendChild(document.createTextNode('€ ' + (product.price).toFixed(2)));

    var removeElement = document.createElement('a');
    removeElement.className = 'cart-product-list__item__remove';
    removeElement.innerHTML = '<i class="cart-product-list__item__remove__icon material-icons">clear</i>';

    removeElement.addEventListener('click', function (e) {
      e.preventDefault();
      handleRemoveClick(itemElement, product);
    });

    itemElement.appendChild(imgElement);
    itemElement.appendChild(detailsElement);
    itemElement.appendChild(priceElement);
    itemElement.appendChild(removeElement);

    $cartProductList.append(itemElement);                      // after the last selection
    $cartProductList.children().last().animate({ height: 'toggle' }, 'fast');
  }
});

$.fn.stepperify = function () {
  var $steppers = $('.stepper');

  function openStepper(index) {
    var $stepper = $steppers.eq(index);
    var $content = $stepper.closest('div').next('.stepper__content');
    $content.addClass('stepper__content--open');
    setStepperActive($stepper);
  }

  function setStepperActive($stepper) {
    $stepper
      .removeClass('stepper--done')
      .addClass('stepper--active');
  }

  function setStepperDone($stepper) {
    $stepper
      .removeClass('stepper--active')
      .addClass('stepper--done');
  }

  function setStepperNotDone($stepper) {
    $stepper
      .removeClass('stepper--active')
      .removeClass('stepper--done');
  }

  function nextStepper ($stepper, $content, stepperIndex) {
    $content.removeClass('stepper__content--open');
    setStepperDone($stepper);
    openStepper(stepperIndex + 1);
  }

  function previouseStepper ($stepper, $content, stepperIndex) {
    $content.removeClass('stepper__content--open');
    setStepperNotDone($stepper);
    openStepper(stepperIndex - 1);
  }

  return this.each(function() {
    var $stepper = $(this);
    var $content = $stepper.closest('div').next('.stepper__content');
    var $buttonNext = $content.find('.button').eq(0);
    var $buttonBack = $content.find('.button').eq(1);
    var stepperIndex = $steppers.index($stepper);

    $buttonNext.on('click', function () {
      switch (stepperIndex) {
        case 0:
          nextStepper($stepper, $content, stepperIndex);
          break;
        case 1:
          var $formCart = $content.find('.form');

          $formCart.on('submit', function (event) {
            event.preventDefault();

            nextStepper($stepper, $content, stepperIndex);
          });
          break;
        case 2:
          var $formCart = $content.find('.form');

          $formCart.on('submit', function (event) {
            event.preventDefault();

            // TODO: create order (order consists of cart & user details)
            // TODO: open chosen payment
          });
          break;
      }
    });

    $buttonBack.on('click', function () {
      previouseStepper($stepper, $content, stepperIndex);
    })
  });
};
