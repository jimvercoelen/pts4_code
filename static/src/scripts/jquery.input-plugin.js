$.fn.inputAnimation = function () {
  return this.each(function() {
    var $input = $(this);
    var $label = $input.find('.input__label');
    var $control = $input.find('.input__control');

    if ($control.val() !== '') {
      $label.css('transition-duration', '0s');
      $label.css('transform', 'scale(0.85714, 0.85714) translateY(-1.35rem)');
    }

    $control.on('focus blur', function () {
      if ($control.val() !== '') {
        $input.toggleClass('input--focus');
      } else {
        $input
          .toggleClass('input--focus')
          .toggleClass('input--floating');
      }
    });

    $input.css('visibility', 'visible');
  });
};

$.fn.hiddenInputHandler = function () {
  return this.each(function () {
    var $input = $(this);
    var $control = $input.find('.input__control');
    var $trigger = $input.find('.input__trigger');
    var $form = $input.find('form');
    var fileSelected = null;

    $trigger.on('click', function () {
      $control.trigger('click');
    });

    $control.on('click', function () {
      fileSelected = this.value;
      this.value = null;
    });

    $control.on('change', function () {
      if (fileSelected != null) {
        $form.submit();
      }
    });
  });
};

$.fn.inputCurrencyHandler = function () {
  return this.each(function () {
    var $input = $(this);
    var min = parseFloat($input.attr('min'));

    $input.change(function () {
      var value = $input.val();
      if (value < min) {
        value = min;
      }

      $input.val(parseFloat(value).toFixed(2));
    });
  });
};

$.fn.checkboxAnimation = function () {
  return this.each(function () {
    var $checkbox = $(this);
    var $control = $checkbox.find('.checkbox__control');
    var $span = $checkbox.find('span:first-child');

    $control.on('click', function () {
      $span.toggleClass('checkbox__circle');
    });
  });
};




