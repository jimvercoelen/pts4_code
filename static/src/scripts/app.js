$(document).ready(function () {

  initializeLocalStorage();

  $('.input').inputAnimation();
  // $('.checkbox').checkboxAnimation();
  $('#menu-trigger').menuHandler();
  $('#backdrop').menuHandler();
  $('.menu__nav--collapse').menuCollapse();
  $('.dropdown').dropdownHandler();
  $('.input--hidden-control').hiddenInputHandler();
  $('.input__control--currency').inputCurrencyHandler();
  $('.stepper').stepperify();
  $('.selection').selectionAnimation();
});


function initializeLocalStorage () {
  var cart = JSON.parse(localStorage.getItem('cart'));
  if (cart == null) {
    cart = {
      products: [],
      totalPrice: 0
    };
  }

  localStorage.setItem('cart', JSON.stringify(cart));
}
