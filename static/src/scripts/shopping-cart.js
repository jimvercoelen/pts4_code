var selectionPrices = [];
var selectionOptions = [];

function addToCart(product) {
  var cart = JSON.parse(localStorage.getItem('cart'));

  cart.products.push(product);
  cart.totalPrice += product.price;

  localStorage.setItem('cart', JSON.stringify(cart));
}


function removeFromCart (product) {
  var cart = JSON.parse(localStorage.getItem('cart'));

  cart.totalPrice -= product.price;
  cart.products = _.filter(cart.products, function (p) {
    return !_.isEqual(p, product)
  });

  localStorage.setItem('cart', JSON.stringify(cart));
}

function setPriceChangedSelection (indexSelection, price) {
  selectionPrices[indexSelection] = price;
}

function clearShoppingCart () {
  selectionPrices = [];
  selectionOptions = [];
}

$(document).ready(function () {
  var $shoppingCart = $('[data-remodal-id=remodal-shopping-cart]');
  var $shoppingCartForm = $shoppingCart.find('.form');
  var $shoppingCartAdd = $shoppingCartForm.find('.shopping-cart__add');
  var $shoppingCartPrice = $shoppingCartForm.find('.shopping-cart__price');
  var $shoppingCartCurrency = $shoppingCartForm.find('.shopping-cart__currency');

  $shoppingCartAdd.on('click', function () {
    handleAddToCart();
  });

  /**
   * When the value of a selection is changed some process needs to be done.
   * This could either be (re)fetching and displaying corresponding product options
   * when the user has chosen a product type.
   * Or (re)fetching an option price and display total price.
   */
  $(document).on('selectionValueChanged', function (event, data) {
    var $selection = data.selection;

    if ($selection.is('#selection-product-type')) {
      var productType = $selection.find('.selection__control').text();
      fetchProductOptions(productType);
    } else {
      var productType = $('#selection-product-type').find('.selection__control').text();
      var optionName = $selection.find('.selection__label').text();
      var optionValue = $selection.find('.selection__control').text();
      var indexChangedSelection = $('.selection').index($selection);
      fetchOptionPrice(productType, optionName, optionValue, indexChangedSelection);
    }
  });

  function fetchProductOptions (productType) {
    $.ajax({
      type: 'GET',
      url: 'http://localhost:9000/product/' + productType
    }).done(function (data) {
      displayOptionCategories(parseDataIntoOptionCategories(data));
    }).fail(function (error) {
      console.log('Error: ', error);
      // TODO: fixe error
      showSnackbar($(this).attr('error'));
    });
  }

  function fetchOptionPrice (productType, optionName, optionValue, indexSelection) {
    $.ajax({
      type: 'GET',
      url: 'http://localhost:9000/product/' + productType + "/" + optionName + "/" + optionValue
    }).done(function (data) {
      setPriceChangedSelection(indexSelection, data);
      displayProductPrice();
    }).fail(function (error) {
      console.log('Error: ', error);
      // TODO: fixe error
      showSnackbar($(this).attr('error'));
    });
  }

  /**
   * Parse the received data (JSON) into
   * an array of categories, each containing all possible options.
   * [
   *  {
   *    category,
   *    options: [ option1, option2, option3 ],
   *  },
   *  {...},
   *  {...}
   * ]
   */
  function parseDataIntoOptionCategories (data) {
    var categories = [];

    for (var i = 0, l = data.length; i < l; i++) {
      var categoryName = data[i].optionName;
      var categoryOption = data[i].optionValue;
      var containsCategory = false;

      for (var j = 0, ll = categories.length; j < ll; j++) {
        if (categories[j].name === categoryName) {
          categories[j].options.push(categoryOption);
          containsCategory = true;
          break;
        }
      }

      if (!containsCategory) {
        categories.push({
          name: categoryName,
          options: [categoryOption]
        });
      }
    }

    return categories;
  }

  /**
   * Display all possible options in the UI (shopping-cart remodal).
   * For each option a selection is appended on the UI.
   */
  function displayOptionCategories (categories) {
    // Remove all selections except the product-type one (first one).
    $('.selection').slice(1).remove();

    // Loop through al categories and append a selection for each one.
    for (var i = 0, l = categories.length; i < l; i++) {
      var category = categories[i];
      var categoryName = category.name;
      var categoryOptions = category.options;
      addCategorySelection(categoryName, categoryOptions);
    }

    // Show currency and price
    $shoppingCartCurrency.show();
    $shoppingCartPrice.show();
  }

  /**
   * Display the total product price in the shopping-cart remodal.
   * This total price depends on all the chosen selections
   * which prices are set in variable called: 'selectionPrices'.
   */
  function displayProductPrice () {
    var totalProductPrice = 0;
    for (var i = 0, l = selectionPrices.length; i < l; i++) {
      var price = selectionPrices[i];
      if (typeof price !== 'undefined') {
        totalProductPrice += price;

      }
    }

    $shoppingCartPrice.text(parseFloat(totalProductPrice).toFixed(2));
  }

  /**
   * Manual adding selection on shopping-cart's from.
   * Selection contains drop down for all options.
   */
  function addCategorySelection(categoryName, categoryOptions) {
    var SelectionElement = document.createElement('div');
    SelectionElement.className = 'selection';
    SelectionElement.style.display = 'none';                                 // set display none so that the element can be animated

    var labelElement = document.createElement('label');
    labelElement.className = 'selection__label';
    labelElement.appendChild(document.createTextNode(categoryName));

    var iconElement = document.createElement('i');
    iconElement.className = 'selection__icon material-icons';
    iconElement.appendChild(document.createTextNode('arrow_drop_down'));

    var controlElement = document.createElement('span');
    controlElement.className = 'selection__control';

    var optionsWrapperElement = document.createElement('ul');
    optionsWrapperElement.className = 'selection__options__wrapper';

    // Loop through all values
    categoryOptions.forEach(function (option) {
      // Option element
      var selectionOption = document.createElement('li');
      selectionOption.className = 'selection__option';
      selectionOption.appendChild(document.createTextNode(option));
      optionsWrapperElement.appendChild(selectionOption);
    });

    // Append elements
    SelectionElement.appendChild(labelElement);
    SelectionElement.appendChild(iconElement);
    SelectionElement.appendChild(controlElement);
    SelectionElement.appendChild(optionsWrapperElement);

    // Add selection to form
    $shoppingCartForm.children('.selection').last().after(SelectionElement);                      // after the last selection
    $shoppingCartForm.children('.selection').last().show('fast');                          // animate selection through show

    // Use of selection-ify which handles some animations
    $shoppingCartForm.children('.selection').last().selectionAnimation();
  }

  function handleAddToCart() {
    var valid = true;
    var product = {};

    product.image = imageUrl;
    product.price = parseFloat($shoppingCartPrice.text());

    $shoppingCart.find('.selection').each(function () {
      var $selection = $(this);
      var $label = $selection.find('.selection__label');
      var $control = $selection.find('.selection__control');
      var field = $label.text();
      var value = $control.text();

      // Break when value of selection is empty
      // (selection is invalid)
      if (value == '') {
        shakeElement($shoppingCart);
        return valid = false;
      }

      product[field.toLowerCase()] = value;
    });

    if (valid) {
      addToCart(product);
      showSnackbar($(this).attr('text'));
      $shoppingCart.remodal().close();
    } else {
      // TODO: fixe error
      showSnackbar($(this).attr('error'));
    }
  }
});

