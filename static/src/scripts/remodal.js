$(document).ready(function () {
  var $photoEditor = $('[data-remodal-id=remodal-photo-editor]');
  var $shoppingCart = $('[data-remodal-id=remodal-shopping-cart]');
  var $shoppingCartEdit = $shoppingCart.find('.form').find('.shopping-cart__edit');

  $photoEditor.on('closing', function (event) {
    $shoppingCart.remodal().open();
  });

  $shoppingCartEdit.on('click', function (event) {
    initializePhotoEditor(imageUrl);
    $photoEditor.remodal().open();
  });

  $shoppingCart.on('closed', function (event) {
    clearSelections();
    clearShoppingCart();
  });

  function clearSelections () {
    $('.selection').slice(1).remove();
    $('#selection-product-type').find('.selection__control').text('');
    $shoppingCart.find('.shopping-cart__price').text('');
    $shoppingCart.find('.shopping-cart__price').hide('');
    $shoppingCart.find('.shopping-cart__currency').hide('');
    $('#selection-product-type')
      .removeClass('selection--floating')
      .removeClass('selection--focus')
      .removeClass('selection--show-options');
  }
});
