var products = [
  {
    type: 'Image',
    price: 5,
    options: [
      { Orientation: ['landscape', 'portrait'] },
      { Sizes: ['9x13cm', '10x15cm', '11x15cm', '13x18cm', '13x19cm', '15x19cm', '15x21cm', '20x10cm', '20x15cm', '20x20cm', '20x25cm'] }
    ]
  },
  {
    type: 'Product2',
    price: 10,
    options: [
      { Optie1: ['mogelijkheid1', 'mogelijkheid2'] },
      { Optie2: ['mogelijkheid1', 'mogelijkheid2', 'mogelijkheid3', 'mogelijkheid4', 'mogelijkheid5'] }
    ]
  }
];
