package models;

import com.avaje.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Product")
public class Product extends Model {

  @Id
  @Column
  public long id;

  @Column
  public String type;

  @Column
  public String optionName;

  @Column
  public String optionValue;

  @Column
  public double price;

  @Column
  public int stock;

  @Column
  public String code;

  @Column
  public String language;

  public Product() { }

  public static Finder<String, Product> find = new Finder<>(Product.class);

  @Override
  public String toString() {
    return type + " " + optionName + " " + optionValue + " " + String.valueOf(price) + " " + String.valueOf(stock);
  }

}
