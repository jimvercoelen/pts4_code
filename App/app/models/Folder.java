package models;

import com.avaje.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

@Entity
@Table(name="Folder")
public class Folder extends Model {

  @Id
  @Column
  public long id;

  @Column
  public String creator;

  @Column
  public String name;

  @Column
  public double price;

  @Transient
  public List<AccessCode> accessCodes;

  @Transient
  public List<FolderAccess> folderAccess;

  public Folder(String creator, String name) {
    this.creator = creator;
    this.name = name;
  }

  public Folder(long id, String creator, String name) {
    this.id = id;
    this.creator = creator;
    this.name = name;
    this.price = 5.00;
  }

  public static Model.Finder<String, Folder> find = new Finder<>(Folder.class);
}
