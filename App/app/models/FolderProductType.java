package models;

import com.avaje.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Folder_product_type")
public class FolderProductType extends Model {

  @Id
  @Column
  public long id;

  @Column
  public long folderId;

  @Column
  public String productType;

  public FolderProductType() {}

  public FolderProductType(long folderId, String productType) {
    this.folderId = folderId;
    this.productType = productType;
  }

  public static Finder<String, FolderProductType> find = new Finder<>(FolderProductType.class);
}
