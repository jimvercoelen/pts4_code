package models;

import com.avaje.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Folder_access")
public class FolderAccess extends Model {

  @Id
  @Column
  public long id;

  @Column
  public long folderId;

  @Column
  public String userEmail;

  public FolderAccess(long folderId, String userEmail) {
    this.folderId = folderId;
    this.userEmail = userEmail;
  }

  public static Finder<String, FolderAccess> find = new Finder<>(FolderAccess.class);
}
