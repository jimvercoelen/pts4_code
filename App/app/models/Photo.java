package models;

public class Photo {
  public final String url;
  public final double width;
  public final double height;

  public Photo(String url, double width, double height) {
    this.url = url;
    this.width = width;
    this.height = height;
  }

  @Override
  public String toString() {
    return url + " " + width + " " + height;
  }
}
