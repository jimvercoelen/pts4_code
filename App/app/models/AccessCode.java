package models;

import com.avaje.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Access_code")
public class AccessCode extends Model {

  @Id
  @Column
  public long id;

  @Column
  public long folderId;

  @Column
  public String accessCode;

  public AccessCode(long folderId, String accessCode) {
      this.folderId = folderId;
      this.accessCode = accessCode;
  }

  public static Finder<String, AccessCode> find = new Finder<>(AccessCode.class);

}
