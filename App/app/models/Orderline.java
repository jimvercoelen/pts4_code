package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.databind.JsonNode;
import util.Constants;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Orderline")
public class Orderline extends Model{

  @Id
  @Column
  public long id;

  @Column
  public long orderid;

  @Column
  public String photourl;

  @Column
  public String options;

  @Column
  public Double price;

  @Column
  public String product;

  public Orderline(JsonNode json) {
    photourl = json.get("photoUrl").asText();
    photourl.replaceAll("/assets/root", Constants.PATH_APP_ROOT_FOLDER);
    options = json.get("options").asText();
    price = json.get("price").asDouble();
    product = json.get("product").asText();
  }

  @Override
  public String toString() {
    return photourl + " " + options + " " + price + " " + product;
  }

  public static Model.Finder<String, Orderline> find = new Finder<>(Orderline.class);
}
