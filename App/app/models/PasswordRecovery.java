package models;

import com.avaje.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Password_recovery")
public class PasswordRecovery extends Model {

  @Id
  @Column
  public int id;

  @Column
  public String email;

  @Column
  public String token;

  public static Finder<String, PasswordRecovery> find = new Finder<>(PasswordRecovery.class);
}
