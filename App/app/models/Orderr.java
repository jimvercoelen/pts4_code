package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.databind.JsonNode;
import helpers.SessionHelper;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Entity
@Table(name="Orderr")
public class Orderr extends Model {

  @Id
  @Column
  public long id;

  @Transient
  public List<Orderline> orderlines;

  @Column
  public String customer;

  @Column
  public double totalprice;

  @Column
  public String city;

  @Column
  public String address;

  @Column
  public String zipcode;

  @Column
  public String phonenumber;

  @Column
  public int processed;

  public Orderr(JsonNode json) {
    customer = json.get("email").textValue();
    totalprice = json.get("totalPrice").doubleValue();
    city = json.get("city").textValue();
    address = json.get("address").textValue();
    zipcode = json.get("zipcode").textValue();
    phonenumber = json.get("phoneNumber").textValue();
    orderlines = new ArrayList<>();
    Iterator<JsonNode> elements = json.get("products").elements();
    while (elements.hasNext()) {
      JsonNode next = elements.next();
      orderlines.add(new Orderline(next));
    }
  }

  @Override
  public String toString() {
    return customer + " " + totalprice + " " + address + " " + zipcode + " " + phonenumber;
  }

  public static Model.Finder<String, Orderr> find = new Finder<>(Orderr.class);
}
