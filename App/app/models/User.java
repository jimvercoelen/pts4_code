package models;

import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="User")
public class User extends Model {

  @Id
  @Column
  public String email;

  @Column
  public String password;

  @Column
  public String userType;

  @Transient
  public boolean remember;

  @Transient
  public List<Folder> folders = new ArrayList<>();

  /** Only beeing used when user is anonymous */
  @Transient
  public AccessCode accessCode;

  public User() { }

  /**
   * Only uses when a user is loged in with a access code
   * (anonymous user)
   * @param accessCode
   */
  public User(AccessCode accessCode) {
    this.accessCode = accessCode;
    this.email = accessCode.accessCode;
    this.userType = "anonymous";
  }

  /**
   * Only for testing purpose
   */
  public User(String email, String password, String userType) {
    this.email = email;
    this.password = password;
    this.userType = userType;
  }

  public static Finder<String, User> find = new Finder<>(User.class);
}


