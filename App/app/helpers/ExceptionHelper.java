package helpers;

import play.Logger;
import play.api.mvc.Call;
import play.mvc.Result;
import util.Snackbar;

import static play.mvc.Results.redirect;

/**
 * Class to handle the exceptions
 */
public class ExceptionHelper {

  /**
   * Handles an exception by logging it and
   * showing a notification through {@link Snackbar#showOops()}
   * @return          redirected call
   */
  public static Result handleException(Exception e, Call call) {
    Logger.error(e.getMessage(), e);
    e.printStackTrace(System.err);
    Snackbar.showOops();
    return redirect(call);
  }

}
