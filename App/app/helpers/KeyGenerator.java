package helpers;

import java.util.Random;

/**
 * Class to create random keys
 */
public class KeyGenerator {

  private static final char[] CHARS = "abcdefghijklmnopqrstuvwxyz".toCharArray();

  /**
   * Method to create a random key
   * @return the random generated key as a String
   */
  public static String generate() {
    StringBuilder sb = new StringBuilder();
    Random random = new Random();
    for (int i = 0; i < 20; i++) {
      char c = CHARS[random.nextInt(CHARS.length)];
      sb.append(c);
    }

    return sb.toString();
  }
}
