package helpers;

import play.mvc.Http;

/**
 * Extra methods for the session
 */
public class SessionHelper {

  private static final String AUTH = "auth";

  /**
   * Method to set the session
   * @param email the mailaddress from the user
   */
  public static void setAuth(String email) {
    Http.Context.current().session().put(AUTH, email.toLowerCase());
  }

  /**
   * Method to get the session
   * @return a string with the data from the session
   */
  public static String getAuth() {
    return Http.Context.current().session().get(AUTH);
  }

  /**
   * Method to remove the session
   */
  public static void removeAuth() {
    Http.Context.current().session().clear();
  }
}
