package helpers;

import models.AccessCode;
import models.User;
import play.Logger;

/**
 * Class with extra methods for authentication
 */
public class AuthHelper {

  /**
   * Method to get the authenticated user
   * @return authenticated user
   */
  public static User getAuth() {
    User auth = null;
    try {
      if (isAuthAnonymous()) {
        AccessCode accessCode = AccessCode.find.where().like("access_code", SessionHelper.getAuth()).findUnique();
        return new User(accessCode);
      } else {
        return User.find.where().like("email", SessionHelper.getAuth()).findUnique();
      }
    } catch (Exception e) {
      Logger.error(e.getMessage(), e);
    }

    return auth;
  }

  /**
   * method when the user is anonymous
   * @return index -1
   */
  private static boolean isAuthAnonymous() {
    String auth = SessionHelper.getAuth();
    return auth.indexOf('@') == -1;
  }
}
