package helpers;

import models.Photo;
import play.mvc.Http;
import util.Constants;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Class to manage the filesystem
 */
public class FileSystemHelper {

  /**
   * Method to create a folder
   * @param rootPath the rootpath from the user
   * @param id the id from the user
   * @throws Exception if something went wrong by creating the folder
   */
  public static void createFolder(String rootPath, String id) throws Exception {
    try {
      String absolutePath = rootPath + "/" + id;
      Path path  = Paths.get(absolutePath);
      if (Files.notExists(path)) {
        new File(absolutePath).mkdir();
      }
    }
    catch (Exception e) {
      throw e;
    }
  }

  /**
   * Method to delete a folder
   * @param rootPath the rootpath from the user
   * @param id the id from the user
   * @throws Exception if something went wrong by deleting the folder
   */
  public static void deleteFolder(String rootPath, String id) throws Exception {
    try {
      String absolutePath = rootPath + "/" + id;
      Path path  = Paths.get(absolutePath);
      File file = path.toFile();
      String[] entries = file.list();

      // Loop through the entire folder and remove all files
      for (String s: entries) {
        File current = new File(file.getPath(), s);
        current.delete();
      }

      // Remove folder
      file.delete();
    } catch(Exception e) {
      throw e;
    }
  }

  /**
   * Method to add a picture in the folder from the user
   * @param pictures one or more pictures
   * @param rootFolder the rootfolder from the user
   * @param folderId the folder id
   * @throws Exception if something went wrong by uploading and write the pictures
   */
  public static void addPhoto(List<Http.MultipartFormData.FilePart<File>> pictures, String rootFolder, String folderId) throws Exception {
    try {
      for (Http.MultipartFormData.FilePart<File> picture : pictures) {
        if (!picture.getFilename().isEmpty()) {
          File file = picture.getFile();
          BufferedImage image = ImageIO.read(file);
          String fileName = picture.getFilename().substring(0, picture.getFilename().indexOf("."));
          DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HHmmss");
          Date date = new Date();
          fileName += " " + dateFormat.format(date) + ".png";
          fileName = fileName.replace(' ', '_');
          String absolutePath = Constants.PATH_APP_ROOT_FOLDER + "/" + rootFolder + "/" + folderId + "/" + fileName;
          File outputFile = new File(absolutePath);
          ImageIO.write(image, "png", outputFile);
        }
      }
    } catch (Exception e) {
      throw e;
    }
  }

  /**
   * Method to delete a picture from the system
   * @param imageUrl the url from the picture
   * @throws Exception if something went wrong deleting the picture
   */
  public static void deletePhoto(String imageUrl) throws Exception {
    try {
      String absolutePath = Constants.PATH_APP_PUBLIC_FOLDER + "/" + imageUrl;
      Path path  = Paths.get(absolutePath);
      Files.delete(path);
    } catch (Exception e) {
      throw e;
    }
  }

  /**
   * Method to get all the photos from a folder
   * @param folderPath the folderpath where the pictures excist
   * @return a list with pictures
   * @throws Exception if something went wrong receiving the pictures
   */
  public static List<Photo> getPhotos(String folderPath) throws Exception {
    List<Photo> photos = new ArrayList<>();
    File[] files = new File(Constants.PATH_APP_ROOT_FOLDER + "/" + folderPath).listFiles();
    return getPhotosRecursive(files, photos);
  }

  /**
   * Method to get all photos
   * @return a list with all pictures
   * @throws Exception if something went wrong receiving the pictures
   */
  public static List<Photo> getAllPhotos() throws Exception {
    File[] files = new File(Constants.PATH_APP_ROOT_FOLDER).listFiles();
    return getPhotosRecursive(files, new ArrayList<>());
  }

  // TODO: Summary
  private static List<Photo> getPhotosRecursive(File[] files, List<Photo> photos) throws IOException {
    for (File file : files) {
      if (file.isDirectory()) {
        getPhotosRecursive(file.listFiles(), photos);
      } else {
        if (file.getName().contains(".png")) {
          photos.add(getPhoto(file));
        }
      }
    }

    return photos;
  }

  /**
   * Method to get one photo
   * @param file the picture to save in the photo class
   * @return the picture as an object or null if something went wrong
   * @throws IOException if something went wrong in the connection
   */
  private static Photo getPhoto(File file) throws IOException {
    try (ImageInputStream in = ImageIO.createImageInputStream(file)) {
      final Iterator<ImageReader> readers = ImageIO.getImageReaders(in);
      if (readers.hasNext()) {
        ImageReader reader = readers.next();
        try {
          reader.setInput(in);
          Dimension dimension = new Dimension(reader.getWidth(0), reader.getHeight(0));
          formatPhotoUrl(file.getPath());
          return new Photo(formatPhotoUrl(file.getPath()), dimension.getWidth(), dimension.getHeight());
        } finally {
          reader.dispose();
        }
      }
    }

    return null;
  }

  /**
   * Method to get the URL format
   * @param folderPath the folder to create the URL
   * @return the URL format from the folder
   */
  private static String formatPhotoUrl(String folderPath) {
    return folderPath.replace(Constants.PATH_APP_PUBLIC_FOLDER, "");
  }
}
