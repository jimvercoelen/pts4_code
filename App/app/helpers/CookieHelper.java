package helpers;

import play.Logger;
import play.mvc.Http;

/**
 * Extra methods for the cookies
 */
public class CookieHelper {

  private static final int DEFAULT_EXPIRE_DATE = 259200; //3d
  private static final String AUTH = "auth";

  /**
   * Method to set the cookie
   * @param email the email from the user
   */
  public static void setAuth(String email) {
    Http.Context.current().response().setCookie(Http.Cookie.builder(AUTH, email.toLowerCase()).withMaxAge(DEFAULT_EXPIRE_DATE).build());
  }

  /**
   * Method to get the value from the cookie
   * @return a string with the value from the cookie
   */
  public static String getAuth() {
    try {
      return Http.Context.current().request().cookies().get("auth").value();
    } catch (Exception e) {
      Logger.error(e.getMessage(), e);
      return null;
    }
  }

  /**
   * Method to remove the cookie
   */
  public static void removeAuth() {
    Http.Context.current().response().discardCookie(AUTH);
  }
}
