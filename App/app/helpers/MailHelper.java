package helpers;

import models.Orderline;
import models.Orderr;
import org.apache.commons.mail.*;
import play.Logger;
import play.i18n.Messages;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Class to manage the mail
 */
public class MailHelper {

  /**
   * Method to set up an e-mail
   * @return the email with settings
   * @throws EmailException if something went wrong writing the settings
   */
  public static Email setEmailSettings() throws EmailException {
    Email email = new SimpleEmail();
    email.setHostName("smtp.gmail.com");
    email.setSmtpPort(465);
    email.setAuthenticator(new DefaultAuthenticator("noreply.proftaak@gmail.com", "Harambe!"));
    email.setSSL(true);
    email.setFrom("noreply.proftaak@gmail.com");
    return email;
  }

  /**
   * Method to send an e-mail with just a little content and a subject
   * @param emailAddress the mailaddress from the receiver
   * @param content the message
   * @param subject the subject from the message
   * @return true if nothing went wrong
   */
  public static boolean send(String emailAddress, String content, String subject) {
    try {
      Email email = setEmailSettings();
      email.setSubject(subject);
      email.setContent(content, "text/html");
      email.addTo(emailAddress);
      email.send();
      return true;
    } catch (EmailException e) {
      Logger.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * Method to send a more complex e-mail, which contains detailed information about the order
   * @param emailAddress the mailaddress from the receiver
   * @param order the order from the customer
   * @param subject the subject from the mail
   * @return true if nothing went wrong
   */
  public static boolean sendOrderMail(String emailAddress, Orderr order, String subject) {
    try {
      Email email = setEmailSettings();
      email.setSubject(subject);
      DecimalFormat formatter = (DecimalFormat)NumberFormat.getCurrencyInstance(Locale.GERMANY);
      DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
      symbols.setCurrencySymbol("");
      formatter.setDecimalFormatSymbols(symbols);
      double totalPrice = 0;

      String content = "<html>";

      for (Orderline orderline : order.orderlines) {
        totalPrice += orderline.price;
        String photoName = orderline.photourl.substring(orderline.photourl.lastIndexOf('/') + 1);
        content += photoName + "<br />" + orderline.product + " - " + orderline.options.replaceAll(", $", "") + "<br /><img src=\"http://i.imgur.com/Tqqf7xX.png\"/>" + formatter.format(orderline.price) + "<br /><br />";
      }

      String title = "<h2>" + Messages.get("orderDetails") + "</h2>";
      String description = Messages.get("mailDescription") + ".<br /><br />";
      //String total = Messages.get("totalprice") + ": <img src=\"http://i.imgur.com/Tqqf7xX.png\"/>" + formatter.format(totalprice) + "</html>";
      String total = Messages.get("totalPrice") + ": <img src=\"http://i.imgur.com/Tqqf7xX.png\"/>" + formatter.format(order.totalprice) + "</html>"; // Deze regel alleen gebruiken als totaalprijs klopt, anders bovenstaande.
      String customerInfo = "<br/ ><br />" + order.customer + "<br />" + order.address + "<br />" + order.zipcode;
      email.setContent(title + description + content + total + customerInfo, "text/html");
      email.addTo(emailAddress);
      email.send();
      return true;
    } catch (EmailException e) {
      Logger.error(e.getMessage(), e);
      return false;
    }
  }
}
