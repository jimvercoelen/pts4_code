package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import helpers.AuthHelper;
import helpers.ExceptionHelper;
import helpers.FileSystemHelper;
import helpers.SessionHelper;
import models.*;
import models.AccessCode;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import sun.misc.BASE64Decoder;
import util.Constants;
import util.Snackbar;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This class is the controller for the folder
 */
public class Folder extends Controller {

  @Inject
  FormFactory formFactory;

  /**
   * An action that renders view {@link views.html.folder}.
   *
   * This method will be called when the application receives a
   * <code>GET</code> request with a path of <code>/folder/:folderId</code>.
   */
  public Result getFolder(long folderId) {
    // Variables
    models.Folder folder;
    List<FolderAccess> folderAccesses;
    User auth;
    String folderPath;
    List<Photo> photos;
    List<models.Product> products;
    List<models.AccessCode> accessCodes;
    HashSet<String> productTypes = new HashSet<>();

    // Get auth
    auth = AuthHelper.getAuth();
    if (auth == null) {
      Snackbar.showOops();
      return redirect(routes.Index.getIndex());
    }

    // Get folder, folder access, access codes, authenticated user and available products from database
    try {
      folder = models.Folder.find.where().like("id", String.valueOf(folderId)).findUnique();
      folder.folderAccess = FolderAccess.find.where().like("folder_id", String.valueOf(folderId)).findList();
      folder.accessCodes = AccessCode.find.where().like("folder_id", String.valueOf(folderId)).findList();

      String currentLanguage = ctx().lang().language();
      products = models.Product.find.where().like("language", currentLanguage.toUpperCase()).findList();

      for(models.Product product : products) {
        if (product.stock > 0) {
          productTypes.add(product.type);
        }
      }
    } catch (Exception e) {
      return ExceptionHelper.handleException(e, routes.Index.getIndex());
    }

    // Get photos from file system
    try {
      folderPath = folder.creator + "/" + String.valueOf(folder.id);
      photos = FileSystemHelper.getPhotos(folderPath);
    } catch (Exception e) {
      return ExceptionHelper.handleException(e, routes.Index.getIndex());
    }

    return ok(views.html.folder.render(folder.name, formFactory.form(), null, folder, auth, photos, productTypes));
  }

  /**
   * An action that creates a new folder.
   *
   * A new folder is saved in the database and in the file system.
   *
   * This method will be called when the application receives a
   * <code>GET</code> request with a path of <code>/folder</code>.
   *
   * @return
   * on success:              renders view {@link views.html.index}.
   * on error:                renders view {@link views.html.index} with a notification.
   */
  public Result getNewFolder() {
    // Variables
    String auth = SessionHelper.getAuth();
    models.Folder newFolder = null;
    FolderAccess folderAccess;
    String rootPath;

    // Save new folder and folder access in the database
    Ebean.beginTransaction();
    try {
      newFolder = new models.Folder(auth, Messages.get("newAlbumTitle"));
      newFolder.save();
      folderAccess = new FolderAccess(newFolder.id, auth);
      folderAccess.save();
    } catch (Exception e) {
      Ebean.rollbackTransaction();
      ExceptionHelper.handleException(e, routes.Index.getIndex());
    }

    // Create new folder on the file system
    try {
      rootPath = Constants.PATH_APP_ROOT_FOLDER + "/" + auth;
      FileSystemHelper.createFolder(rootPath, String.valueOf(newFolder.id));
    } catch (Exception e) {
      Ebean.rollbackTransaction();
      ExceptionHelper.handleException(e, routes.Index.getIndex());
    }

    // Show succeed notification and commit transaction
    Snackbar.showNotification(Messages.get("newFolder"));
    Ebean.commitTransaction();

    // Redirect back to index (which should show the new album on it's turn)
    return redirect(routes.Index.getIndex());
  }

  /**
   * An action that deletes the album if succeeded (shows an error if failed).
   *
   * This method will be called when the application receives a
   * <code>POST</code> request with a path of <code>/folder/:folderId</code>.
   */
  public Result deleteFolder(long folderId) {
    // Variables
    String email = SessionHelper.getAuth();
    String rootPath = Constants.PATH_APP_ROOT_FOLDER + "/" + email;

    // Delete folder and all folder accesses from the database
    Ebean.beginTransaction();
    try {
      models.Folder folder = models.Folder.find.where().like("id", String.valueOf(folderId)).findUnique();
      List<FolderAccess> folderAccesses = FolderAccess.find.where().like("folder_id", String.valueOf(folderId)).findList();
      folderAccesses.forEach(Model::delete);
      folder.delete();
    } catch (Exception e) {
      Ebean.rollbackTransaction();
      ExceptionHelper.handleException(e, routes.Index.getIndex());
    }

    // Delete folder from file system
    try {
      FileSystemHelper.deleteFolder(rootPath, String.valueOf(folderId));
    } catch (Exception e) {
      Ebean.rollbackTransaction();
      ExceptionHelper.handleException(e, routes.Index.getIndex());
    }

    // Show succeed notification and commit transaction
    Snackbar.showNotification(Messages.get("removedFolder"));
    Ebean.commitTransaction();

    return redirect(routes.Index.getIndex());
  }

  /**
   * An action that changes a folders settings if succeeded
   * (shows an error if failed).
   *
   * This method will be called when the application receives a
   * <code>POST</code> request with a path of <code>/folder/settings/:folderId</code>.
   */
  public Result postFolderSettings(long folderId) {
    // Variables
    DynamicForm requestData = formFactory.form().bindFromRequest();
    String name = requestData.get("name");
    double price = Double.parseDouble(requestData.get("price"));
    Map<String, String[]> map = request().body().asFormUrlEncoded();
    String[] chosenProductTypes = map.get("productType");

    // Database call for update, delete and save
    try {
      // Update folder
      models.Folder folder = models.Folder.find.where().like("id", String.valueOf(folderId)).findUnique();
      folder.name = name;
      folder.price = price;
      folder.save();

      // Delete all (old) chosen product types (if there are any)
      List<FolderProductType> folderProductTypes = FolderProductType.find.where().like("folder_id", String.valueOf(folderId)).findList();
      folderProductTypes.forEach(Model::delete);

      // Save all (new) chosen product types
      for (int i = 0; i < chosenProductTypes.length; i++) {
        FolderProductType folderProductType = new FolderProductType(folderId, chosenProductTypes[i]);
        folderProductType.save();
      }

    } catch (Exception e) {
      ExceptionHelper.handleException(e, routes.Folder.getFolder(folderId));
    }

    // Show succeed notification
    Snackbar.showNotification(Messages.get("saved"));

    return redirect(routes.Folder.getFolder(folderId));
  }

  /**
   * This method creates a shared folder for multiple users
   * @param folderId The folder id from the folder
   * @return the webpage with the folder
   */
  public Result postFolderShare(long folderId) {
    // Variables
    DynamicForm requestData = formFactory.form().bindFromRequest();
    String email = requestData.get("email");
    FolderAccess folderAccess = new FolderAccess(folderId, email);

    // Save new folder access in the database
    try {
      folderAccess.save();
      Snackbar.showNotification(Messages.get("sharedFolder"));
    } catch (Exception e) {
      // Notifying the user whats wrong depending on the Exception
      if (e.getMessage().contains("foreign key constraint fails")) {
        Snackbar.showNotification(Messages.get("unknownEmail"));
      } else if (e.getMessage().contains("Duplicate entry")) {
        Snackbar.showNotification(Messages.get("sharedFolder"));
      } else {
        Snackbar.showOops();
      }
    }

    return redirect(routes.Folder.getFolder(folderId));
  }

  /**
   * An action that deletes the share if succeeded (shows an error if failed).
   *
   * This method will be called when the application receives a
   * <code>GET</code> request with a path of <code>/folder/share/:folderId/:email</code>.
   */
  public Result deleteFolderShare(long folderId, String email) {
    // Delete folder access from database
    try {
      FolderAccess folderAccess = FolderAccess.find.where()
        .like("folder_id", String.valueOf(folderId))
        .like("user_email", email)
        .findUnique();
      folderAccess.delete();
    } catch (Exception e) {
      ExceptionHelper.handleException(e, routes.Folder.getFolder(folderId));
    }

    // Show succeed notification
    Snackbar.showNotification(Messages.get("unSharedFolder"));

    return redirect(routes.Folder.getFolder(folderId));
  }

  /**
   * An action that uploads a photo and renders the folder {@link views.html.folder} if succeeded
   * (shows an error if failed).
   *
   * This method will be called when the application receives a
   * <code>POST</code> request with a path of <code>/folder/:folderId/photo</code>.
   */
  public Result postPhoto(long folderId) {
    // Variables
    Http.MultipartFormData<File> body = request().body().asMultipartFormData();
    List<Http.MultipartFormData.FilePart<File>> pictures = body.getFiles();
    String auth = SessionHelper.getAuth();

    // Save photos in the file system
    try {
      FileSystemHelper.addPhoto(pictures, auth, String.valueOf(folderId));
    } catch (Exception e) {
      ExceptionHelper.handleException(e, routes.Folder.getFolder(folderId));
    }

    // Tmp solution
    // TODO: use of a callback or something
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      ExceptionHelper.handleException(e, routes.Folder.getFolder(folderId));
    }

    // Show succeed notification
    Snackbar.showNotification(Messages.get("photoAdded"));

    return redirect(routes.Folder.getFolder(folderId));
  }

  /**
   * This method post an edited photo
   * @param folderId the folder with the edited picture
   * @return the folder with the picture
   */
  public Result postPhotoEdit(long folderId) {
    // Variable
    DynamicForm form = formFactory.form().bindFromRequest();
    Map<String, String> m = form.data();
    BufferedImage image = null;
    byte[] imageByte;

    for(Map.Entry<String, String> entry : m.entrySet()) {
      String s = entry.getValue().replaceAll("data:image/png;base64,", "");

      // Create a buffered image
      try {
        BASE64Decoder decoder = new BASE64Decoder();
        imageByte = decoder.decodeBuffer(s);
        ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
        image = ImageIO.read(bis);
        long time = System.currentTimeMillis();
        String path = Constants.PATH_APP_ROOT_FOLDER;
        String email = SessionHelper.getAuth();
        File outputfile = new File(path + "/" + email + "/" + time + ".png");
        ImageIO.write(image, "png", outputfile);

        bis.close();
      } catch (IOException e) {
        ExceptionHelper.handleException(e, routes.Folder.getFolder(folderId));
      }
    }

    // Note: succeed notification is shown by JavaScript

    return redirect(routes.Folder.getFolder(folderId));
  }

  /**
   * method to delete a picture from a folder
   * @param folderId the folder id where the picture is saved
   * @param photoUrl the url from the picture
   * @return the folder where the picture was previously
   */
  public Result deletePhoto(long folderId, String photoUrl) {
    // Delete photo from the file system
    try {
      FileSystemHelper.deletePhoto(photoUrl);
    } catch (Exception e) {
      ExceptionHelper.handleException(e, routes.Folder.getFolder(folderId));
    }

    // Show succeed notification
    Snackbar.showNotification(Messages.get("photoRemoved"));

    return redirect(routes.Folder.getFolder(folderId));
  }

}
