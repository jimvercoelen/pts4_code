package controllers;

import helpers.AuthHelper;
import helpers.SessionHelper;
import models.Folder;
import models.FolderAccess;
import models.User;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import security.Authentication;
import util.Snackbar;
import views.html.index;

import java.util.List;
/**
 * This class is the controller for the index
 */
public class Index extends Controller {

  /**
   * An action that renders view {@link views.html.index}.
   *
   * This view is only accessible if the user is authenticated {@see Authentication}.
   *
   * This method will be called when the application receives a
   * <code>GET</code> request with a path of <code>/</code>.
   */
  @Security.Authenticated(Authentication.class)
  public Result getIndex() {
    // Get auth
    User auth = AuthHelper.getAuth();
    if (auth == null) {
      return redirect(routes.SignIn.getSignIn());
    }

    // If auth is anonymous redirect directly to folder
    if (auth.userType.equals("anonymous")) {
      return redirect(routes.Folder.getFolder(auth.accessCode.folderId));
    }

    // If auth is admin redirect directly to admin
    if (auth.userType.equals("admin")) {
      return redirect(routes.Admin.getPhotos());
    }

    // Get folders and redirect to index (containing all folders enstuff)
    try {
      List<FolderAccess> folderAccesses = FolderAccess.find.where().like("user_email", auth.email).findList();
      for (FolderAccess folderAccess : folderAccesses) {
        Folder folder = Folder.find.where().like("id", String.valueOf(folderAccess.folderId)).findUnique();
        if (folder != null) {                   // <- kan deze ooit null zijn? anders krijg hij niks gevonden toch (not sure)
          auth.folders.add(folder);
        }
      }

      return ok(index.render("Index", auth));
    } catch (Exception e) {
      Snackbar.showOops();
      return redirect(routes.SignIn.getSignIn());
    }
  }

  /**
   * An action that sets the language of the application.
   *
   * This method will be called when the application receives a
   * <code>GET</code> request with a path of <code>/language/{language}</code>.
   */
  public Result setLanguage(String language) {
    ctx().changeLang(language);
    return redirect(routes.Index.getIndex());
  }
}
