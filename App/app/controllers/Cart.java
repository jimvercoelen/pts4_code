package controllers;

import helpers.AuthHelper;
import models.User;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.*;

/**
 * This class controls the shopping cart
 */
public class Cart extends Controller {

  /**
   * Get the shoppingcart from a specified user
   * @return the shoppingcart with the products that the user want to order
   */
  public Result getCart() {
    User auth = AuthHelper.getAuth();
    if (auth == null) {
      redirect(routes.Index.getIndex());
    }

    return ok(cart.render(Messages.get("shoppingCart"), auth));
  }
}
