package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.fasterxml.jackson.databind.JsonNode;
import helpers.MailHelper;
import models.Orderline;
import models.Orderr;
import play.Logger;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import util.Snackbar;

/**
 * This class is the controller for the order
 */
public class Order extends Controller {

  /**
   * Method to post an order
   * @return a message if the order was successful
   */
  public Result postOrder() {
    JsonNode json = request().body().asJson();
    if (json == null) {
      return badRequest("No Json data");
    }

    Orderr order = new Orderr(json);
    Ebean.beginTransaction();
    try {
      order.save();
      for (Orderline orderLine : order.orderlines) {
        orderLine.orderid = order.id;
        orderLine.save();
      }
//      Ebean.commitTransaction();
//      Ebean.beginTransaction();

      if (MailHelper.sendOrderMail(order.customer, order, Messages.get("orderMade"))) {
        Ebean.commitTransaction();
      } else {
        Snackbar.showOops();
        Ebean.rollbackTransaction();
      }

      return ok("Order has been saved");
    } catch (Exception e) {
      Ebean.rollbackTransaction();
      Logger.error(e.getMessage(), e);
      return badRequest("Order could not be saved");
    } finally {
      Ebean.endTransaction();
    }
  }
}
