package controllers;

import play.Logger;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import util.Snackbar;

/**
 * This class controls the AccessCode from the folders
 */
public class AccessCode extends Controller {

  /**
   * This method receives the access code from a folder and save this the database
   * @param folderId the ID from the folder
   * @param newAccessCode the required accesscode
   * @return the result from the method, successful or unsuccessful
   */
  public Result getAccessCode(long folderId, String newAccessCode) {
    models.AccessCode accessCode = new models.AccessCode(folderId, newAccessCode);

    // Save new access code into the database
    try {
      accessCode.save();

      return ok();
    } catch (Exception e) {
      Logger.error(e.getMessage(), e);
      return badRequest();
    }
  }

  /**
   * Delete an excisting accesscode from the database
   * @param folderId the id from the folder
   * @param accessCodeId the accesscode that belongs to the folder
   * @return the result from the method, successful or unsuccessful
   */
  public Result deleteAccessCode(long folderId, long accessCodeId) {
    models.AccessCode accessCode = models.AccessCode.find.where().like("id", String.valueOf(accessCodeId)).findUnique();

    // Delete access code from the database
    try {
      accessCode.delete();
      Snackbar.showNotification(Messages.get("accessCode.delete"));
    } catch (Exception e) {
      Logger.error(e.getMessage(), e);
      Snackbar.showOops();
    }

    return redirect(routes.Folder.getFolder(folderId));
  }
}
