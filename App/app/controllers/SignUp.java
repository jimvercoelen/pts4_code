package controllers;

import com.avaje.ebean.Ebean;
import com.google.inject.Inject;
import helpers.FileSystemHelper;
import helpers.SessionHelper;
import models.User;
import play.Logger;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import util.Constants;
import util.Snackbar;
import views.html.*;

import java.util.HashMap;
import java.util.Map;

/**
 * This class is the controller for signing up on the website
 */
public class SignUp extends Controller {

  @Inject
  FormFactory formFactory;

  /**
   * An action that renders view {@link views.html.signup}.
   *
   * This method will be called when the application receives a
   * <code>GET</code> request with a path of <code>/signup</code>.
   */
  public Result getSignUp() {
    return ok(signup.render(Messages.get("signUp"), formFactory.form(), null));
  }

  /**
   * An action that saves an {@link User} instance into the database.
   *
   * When the user is successfully registered a session will be set
   * containing the user's email as value.
   *
   * This method will be called when the application receives a
   * <code>POST</code> request with a path of <code>/signup</code>.
   *
   * @return
   * on success:              renders view {@link views.html.index}.
   * on error:                renders view {@link views.html.signin} with an error message.
   */
  public Result postSignUp() {
    // Variables
    DynamicForm requestData = formFactory.form().bindFromRequest();
    String email = requestData.get("email");
    String password = requestData.get("password");
    String userType = requestData.get("userType");
    Map<String, String> errors = new HashMap<>();
    User newUser = new User();
    newUser.email = email.toLowerCase();
    newUser.password = password;
    newUser.userType = userType;

    Ebean.beginTransaction();
    try {
      // Save user (in db)
      newUser.save();

      // Create user folder
      FileSystemHelper.createFolder(Constants.PATH_APP_ROOT_FOLDER, email);

      // Set authentication session
      SessionHelper.setAuth(newUser.email);
      Ebean.commitTransaction();
      return redirect(routes.Index.getIndex());

    } catch (Exception e) {
      Logger.error(e.getMessage(), e);

      // Notifying the user whats wrong depending on the Exception
      if (e.getMessage().contains("Duplicate entry")) {
        errors.put("email", Messages.get("error.duplicated.entry"));
        return badRequest(signup.render(Messages.get("signUp"), requestData, errors));
      }

      Snackbar.showOops();
      Ebean.commitTransaction();
      return badRequest(signup.render(Messages.get("signUp"), requestData, errors));
    } finally {
      Ebean.endTransaction();
    }
  }
}
