package controllers;

import com.avaje.ebean.Ebean;
import helpers.ExceptionHelper;
import helpers.FileSystemHelper;
import helpers.SessionHelper;
import models.*;
import models.Product;
import play.Logger;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import util.Constants;
import util.Snackbar;

import javax.inject.Inject;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Admin extends Controller {

  @Inject
  FormFactory formFactory;

  private List<models.Product> products;
  private DecimalFormat formatter;

  // TODO: Summary
  public Result getPhotos() {
    // Variables
    List<Photo> photos;

    // Get all photos
    try {
      photos = FileSystemHelper.getAllPhotos();
      return ok(views.html.admin.photos.render("Admin", photos));
    } catch (Exception e) {
      return ExceptionHelper.handleException(e, routes.Admin.getPhotos());
    }
  }

  // TODO: Summary
  public Result deletePhoto(String photoUrl) {
    // Delete photo from the file system
    try {
      FileSystemHelper.deletePhoto(photoUrl);
    } catch (Exception e) {
      return ExceptionHelper.handleException(e, routes.Admin.getPhotos());
    }

    // Show succeed notification
    Snackbar.showNotification(Messages.get("photoRemoved"));
    return redirect(routes.Admin.getPhotos());
  }

  // Method to get all orders from database and show the admin the correct page
  public Result getOrders() {
    DecimalFormat formatter = (DecimalFormat) NumberFormat.getCurrencyInstance(Locale.GERMANY);
    DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
    symbols.setCurrencySymbol("");
    formatter.setDecimalFormatSymbols(symbols);

    List<Orderr> orders = Orderr.find.all();

    for(Orderr order : orders) {
      order.orderlines = Orderline.find.where().like("orderid", String.valueOf(order.id)).findList();
    }

    return ok(views.html.admin.orders.render("Admin", orders, formatter));
  }

  // Method to set the value of processed. If an order is processed it will be 1, if not, 0.
  public Result setProcessed(long orderId) {
    try {
      Orderr order = Orderr.find.where().like("id", String.valueOf(orderId)).findUnique();
      if (order.processed == 0) {
        order.processed = 1;
      }
      else {
        order.processed = 0;
      }
      order.save();
    } catch (Exception e) {
      ExceptionHelper.handleException(e, routes.Admin.getOrders());
    }
    return redirect(routes.Admin.getOrders());
  }

  // Method to get all product from database and show the admin the correct page
  public Result getProducts() {
    products = Product.find.all();
    formatter = (DecimalFormat) NumberFormat.getCurrencyInstance(Locale.GERMANY);
    DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
    symbols.setCurrencySymbol("");
    formatter.setDecimalFormatSymbols(symbols);
    return ok(views.html.admin.products.render("Admin", formFactory.form(), products, ctx().lang().code().toUpperCase(), formatter));
  }

  // Method to change the price and stock of a product
  public Result changePriceAndStock(String code)
  {
    DynamicForm requestData = formFactory.form().bindFromRequest();
    double newPrice = Double.parseDouble(requestData.get("price" + code));
    int newStock = Integer.parseInt(requestData.get("stock" + code));

    // Database call for update
    try {
        List<models.Product> productsToUpdate = Product.find.where().like("code", code).findList();
        for(Product product : productsToUpdate) {
          product.price = newPrice;
          product.stock = newStock;
          product.save();
        }
    } catch (Exception e) {
      ExceptionHelper.handleException(e, routes.Admin.getProducts());
    }
    return redirect(routes.Admin.getProducts());
  }
}
