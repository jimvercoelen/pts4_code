package controllers;

import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import util.Snackbar;

import java.util.List;

public class Product extends Controller {

  // TODO: Summary
  public Result getProductOptions(String productType) {
    response().setHeader("Access-Control-Allow-Origin", "*");
    String currentLanguage = ctx().lang().language();

    try {
      List<models.Product> products = models.Product.find.where()
        .like("type", productType)
        .like("language", currentLanguage)
        .findList();
      return ok(Json.toJson(products));
    } catch (Exception e) {
      Logger.error(e.getMessage(), e);
      Snackbar.showOops();
      return badRequest();
    }
  }

  // TODO: Summary
  public Result getProductOptionPrice(String productType, String productOption, String productValue) {
    response().setHeader("Access-Control-Allow-Origin", "*");
    String currentLanguage = ctx().lang().language();

    try {
      models.Product product = models.Product.find.where()
        .like("type", productType)
        .like("option_name", productOption)
        .like("option_value", productValue)
        .like("language", currentLanguage)
        .findUnique();
      return ok(Json.toJson(product.price));
    } catch (Exception e) {
      Logger.error(e.getMessage(), e);
      Snackbar.showOops();
      return badRequest();
    }
  }
}
