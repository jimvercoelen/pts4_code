package controllers;

import helpers.KeyGenerator;
import helpers.MailHelper;
import models.PasswordRecovery;
import models.User;
import play.Logger;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import util.Snackbar;
import views.html.*;

import javax.inject.Inject;

/**
 * This class is the controller for passwords
 */
public class Password extends Controller {

  private static final String BASE_URL = "http://www.localhost:9000/";

  @Inject
  FormFactory formFactory;

  /**
   * An action that renders view {@link views.html.password_forgotten}.
   *
   * This method will be called when the application receives a
   * <code>GET</code> request with a path of <code>/password-forgotten</code>.
   */
  public Result getPasswordForgotten() {
    return ok(password_forgotten.render("password-forgotten", formFactory.form(), null));
  }

  /**
   * An action that saves a {@link PasswordRecovery} instance into the database
   * and sends the user's an email with a link to {@link #getPasswordRecovery(String, String)}
   *
   * This method will be called when the application receives a
   * <code>POST</code> request with a path of <code>/password-forgotten</code>.
   *
   * @return
   * on success:              renders view {@link views.html.signin}.
   * on error:                renders view {@link views.html.password_forgotten} with an error message.
   */
  public Result postPasswordForgotten() {
    // Variables
    DynamicForm requestData = formFactory.form().bindFromRequest();
    System.out.println(requestData);
    String email = requestData.get("email");
    System.out.println(email);
    String token = KeyGenerator.generate();
    PasswordRecovery passwordRecovery;
    String mailContent;
    String error = null;

    try {
      // Check is user exists
      if (User.find.where().like("email", email).findUnique() != null) {
        passwordRecovery = new PasswordRecovery();
        passwordRecovery.email = email;
        passwordRecovery.token = token;
        passwordRecovery.save();

        // Send mail
        String link = BASE_URL + "password-recovery/" + email + "/" + token;
        mailContent = "<html>" + Messages.get("click") + " <a href=" + link + ">" + Messages.get("here") + "</a> " + Messages.get("clickLink") +".</html>";
        if (MailHelper.send(email, mailContent, Messages.get("mailSubject"))) {
          Snackbar.showNotification(Messages.get("password.forgotten.notification"));
          return redirect(routes.SignIn.getSignIn());
        } else {
          Snackbar.showOops();
        }
      } else {
        error = Messages.get("email.unknown");
      }
    } catch (Exception e) {
      Logger.error(e.getMessage(), e);
      Snackbar.showOops();
    }

    return badRequest(password_forgotten.render("password-forgotten", requestData, error));
  }

  /**
   * An action that renders view {@link views.html.password_recovery} when the application receives a
   * <code>GET</code> request with a path of <code>/password-recovery/{email}/{token}</code>.
   *
   * This view is only accessible when the <code>email</code> and <code>token</code> combination
   * of the links parameters exists in the database.
   *
   * An user will receive this link only by mail.
   * This ensures that only the owner of an user account can change his/hers password.
   *
   * The user will be redirected to {@link views.html.password_forgotten}
   * when the <code>email</code> and <code>token</code> combination doesn't exist
   * or when an exception is thrown.
   */
  public Result getPasswordRecovery(String email, String token) {
    try {
      PasswordRecovery passwordRecovery = PasswordRecovery.find.where().like("email", email).like("token", token).findUnique();
      if (passwordRecovery != null) {
        passwordRecovery.delete();
        return ok(password_recovery.render("password-recovery", email));
      } else {
        Snackbar.showNotification(Messages.get("password.recovery.expired"));
        return redirect(routes.Password.getPasswordForgotten());
      }
    } catch (Exception e) {
      Logger.error(e.getMessage(), e);
      Snackbar.showOops();
      return redirect(routes.Password.getPasswordForgotten());
    }
  }

  /**
   * An action that updates an users password into the databases.
   *
   * This method will be called when the application receives a
   * <code>POST</code> request with a path of <code>/password-recovery</code>.
   *
   * The body of the request will contain an email and password of the user.
   *
   * @return
   * on success:        renders view {@link views.html.signin}.
   * on error:          renders view {@link views.html.password_recovery} with an error message.
   */
  public Result postPasswordRecovery() {
    // Variables
    DynamicForm requestData = formFactory.form().bindFromRequest();
    String email = requestData.get("email");
    String password = requestData.get("password");
    User user;

    try {
      // Find user and update password
      user = User.find.where().like("email", email).findUnique();
      user.password = password;
      user.update();
      Snackbar.showNotification(Messages.get("password.recovery.notification"));
      return redirect(routes.SignIn.getSignIn());
    } catch (Exception e) {
      Logger.error(e.getMessage(), e);
      Snackbar.showOops();
      return badRequest(password_recovery.render("password-recovery", email));
    }
  }
}
