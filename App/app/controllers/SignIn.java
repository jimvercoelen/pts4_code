package controllers;

import helpers.CookieHelper;
import helpers.SessionHelper;
import models.*;
import models.AccessCode;
import play.Logger;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import util.Snackbar;
import views.html.*;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is the controller for logging in to the website
 */
public class SignIn extends Controller {

  @Inject
  FormFactory formFactory;

  /**
   * An action that renders view {@link views.html.signin}.
   *
   * This method will be called when the application receives a
   * <code>GET</code> request with a path of <code>/signin</code>.
   */
  public Result getSignIn() {
    return ok(signin.render(Messages.get("signIn"), formFactory.form(), null));
  }

  /**
   * An action that authenticates an user.
   *
   * When authentication is successful a session will be set with the email
   * of the user as value. A cookie with the same value will be set when the
   * user has chosen to remember his/hers sign in credentials.
   *
   * TODO: explain the code login shit
   *
   * This method will be called when the application receives a
   * <code>POST</code> request with a path of <code>/signin</code>.
   *
   * @return
   * on success:              renders view {@link views.html.index}.
   * on error:                renders view {@link views.html.signin} with an error message.
   */
  public Result postSignIn() {
    // Variables
    DynamicForm requestData = formFactory.form().bindFromRequest();
    String email = requestData.get("email").toLowerCase();
    String password = requestData.get("password");
    String code = requestData.get("accessCode");
    String remember = requestData.get("remember");
    Map<String, String> errors = new HashMap<>();

    // Check login (from db)
    try {
      // User credentials login
      User user = User.find.where().like("email", email).like("password", password).findUnique();
      if (user != null) {
        // Set authentication cookie AND OR session
        if (remember != null && remember.equals("true")) CookieHelper.setAuth(email);
        SessionHelper.setAuth(email);

        return redirect(routes.Index.getIndex());
      } else {
        // Access code login
        AccessCode accessCode = AccessCode.find.where().like("access_code", code).findUnique();
        if (accessCode != null) {
          // Set authentication cookie AND OR session
          if (remember != null && remember.equals("true")) CookieHelper.setAuth(email);
          SessionHelper.setAuth(code);

          return redirect(routes.Index.getIndex());
        }

        // No login found?
        // Then Set errors and return sign in view
        errors.put("accessCode", Messages.get("accessCode.unknown"));
        errors.put("email", Messages.get("incorrect.email"));
        errors.put("password", Messages.get("incorrect.password"));
        return badRequest(signin.render(Messages.get("signIn"), requestData, errors));
      }
    } catch (Exception e) {
      Logger.error(e.getMessage(), e);
      Snackbar.showOops();
      return badRequest(signin.render(Messages.get("signIn"), requestData, errors));
    }
  }
}
