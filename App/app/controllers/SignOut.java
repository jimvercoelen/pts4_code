package controllers;

import helpers.CookieHelper;
import helpers.SessionHelper;
import play.mvc.Controller;
import play.mvc.Result;

/**
 * This class is the controller for signing out from the website
 */
public class SignOut extends Controller {

  /**
   * An action that removes authentication in session and cookie and
   * redirects to route of {@link SignIn#getSignIn()}.
   *
   * This method will be called when the application receives a
   * <code>GET</code> request with a path of <code>/signout</code>.
   */
  public Result signOut() {
    CookieHelper.removeAuth();
    SessionHelper.removeAuth();
    return redirect(routes.SignIn.getSignIn());
  }

}
