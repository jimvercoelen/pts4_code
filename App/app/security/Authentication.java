package security;

import controllers.routes;
import helpers.CookieHelper;
import helpers.SessionHelper;
import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.Security;

/**
 * Class to manage the authentication
 */
public class Authentication extends Security.Authenticator {

  /**
   * Method to get the username
   * @param ctx
   * @return a string with the cookie data
   */
  @Override
  public String getUsername(Context ctx) {

    // Check auth in session
    String auth = SessionHelper.getAuth();
    if (auth != null) return auth;

    // Check auth in cookie
    else return CookieHelper.getAuth();
  }

  /**
   * Method to redirect unauthorizzed access to the sign in page
   * @param ctx
   * @return
   */
  @Override
  public Result onUnauthorized(Context ctx) {
    return redirect(routes.SignIn.getSignIn());
  }
}
