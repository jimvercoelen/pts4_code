package util;

import play.i18n.Messages;

public class Snackbar {

  public static boolean show;
  public static String notification;

  public static void showOops() {
    Snackbar.show = true;
    Snackbar.notification = Messages.get("error.oops");
  }

  public static void showNotification(String notification) {
    Snackbar.show = true;
    Snackbar.notification = notification;
  }

  public static boolean showNotification() {
    if (show) {
      show = false;
      return true;
    }

    return false;
  }
}
