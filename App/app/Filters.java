import filters.LoggingFilter;
import play.Environment;
import play.filters.cors.CORSFilter;
import play.http.DefaultHttpFilters;
import play.mvc.EssentialFilter;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class Filters extends DefaultHttpFilters {

  private final Environment env;
  private final EssentialFilter loggingFilter;

  @Inject
  public Filters(Environment env, LoggingFilter logginFilter, CORSFilter corsFilter) {
    super(corsFilter);
    this.env = env;
    this.loggingFilter = logginFilter;
  }

  @Override
  public EssentialFilter[] filters() {
//    if (env.mode().equals(Mode.DEV) || env.mode().equals(Mode.TEST)) {
//      return new EssentialFilter[] { loggingFilter };
//    } else {
//      return new EssentialFilter[] {};
//    }
    return new EssentialFilter[] { };
  }
}
