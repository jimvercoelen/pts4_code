package controllers;

import org.junit.Before;
import org.junit.Test;
import play.mvc.Result;
import play.test.WithApplication;

import static org.asynchttpclient.util.HttpConstants.Methods.GET;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static play.mvc.Http.Status.SEE_OTHER;
import static play.test.Helpers.*;

public class SignOutTest extends WithApplication {

  @Before
  public void setUp() {
    start(fakeApplication());
  }

  /**
   * Testing behaviour of action {@link SignUp#getSignUp()}.
   *
   * Result status is expected to be {@code SEE_OTHER} (303).
   * There is no session expected as wel.
   */
  @Test
  public void testGetSignUp() {
    Result result = route(fakeRequest(GET, "/signout"));

    assertEquals(SEE_OTHER, result.status());
    assertNull(result.session().get("auth"));
  }

}
