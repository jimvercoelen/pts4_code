package controllers;

import com.avaje.ebean.Ebean;
import models.User;
import org.junit.Before;
import org.junit.Test;
import play.mvc.Result;
import play.test.WithApplication;

import static org.junit.Assert.assertEquals;
import static play.test.Helpers.*;

public class FolderTest extends WithApplication {

  @Before
  public void setUp() {
    start(fakeApplication(inMemoryDatabase()));
    Ebean.save(new User("bob@example.com", "secret", "customer"));
  }

  /**
   * Testing behaviour of action {@link Folder#postPhoto(long)}}, without adding a picture.
   *
   * Result status is expected to be {@code BAD_REQUEST} (404).
   */
  @Test
  public void testUploadInvalid() {
    Result result = route(fakeRequest(POST, "/postPhoto"));
    System.out.println(result.status());
    assertEquals(404, result.status());
  }
}
