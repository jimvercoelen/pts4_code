package controllers;

import com.avaje.ebean.Ebean;
import models.User;
import org.junit.Before;
import org.junit.Test;
import play.mvc.Result;
import play.test.WithApplication;

import java.util.HashMap;
import java.util.Map;

import static org.asynchttpclient.util.HttpConstants.Methods.GET;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.*;

public class SignUpTest extends WithApplication {

  @Before
  public void setUp() {
    start(fakeApplication(inMemoryDatabase()));
    Ebean.save(new User("bob@example.com", "secret", "photographer"));
  }

  /**
   * Testing behaviour of action {@link SignUp#getSignUp()}.
   *
   * Result status is expected to be {@code OK} (200).
   */
  @Test
  public void testGetSignUp() {
    Result result = route(fakeRequest(GET, "/signup"));

    assertEquals(OK, result.status());
    assertEquals("text/html", result.contentType().get());
    assertEquals("utf-8", result.charset().get());
  }

  /**
   * Testing behaviour of action {@link SignUp#postSignUp()}
   * with valid {@code body} values (login credentials).
   *
   * Result status is expected to be {@code SEE_OTHER} (303).
   * There is no session expected as well.
   */
  @Test
  public void testPostSignUpValid() {
    Map<String, String> body = new HashMap<>();
    body.put("email", "test@example.com");
    body.put("password", "secret");
    body.put("userType", "customer");

    Result result = route(fakeRequest(POST, "/signup").bodyForm(body));

    assertEquals(SEE_OTHER, result.status());
    assertEquals("test@example.com", result.session().get("auth"));
  }

  /**
   * Testing behaviour of action {@link SignUp#postSignUp()}
   * with invalid {@code body} values (login credentials).
   *
   * Result status is expected to be {@code BAD_REQUEST} (400).
   * There is no session expected as well.
   */
  @Test
  public void testPostSignUpInValid() {
    Map<String, String> body = new HashMap<>();
    body.put("email", "bob@example.com");
    body.put("password", "secret");
    body.put("userType", "customer");

    Result result = route(fakeRequest(POST, "/signup").bodyForm(body));

    assertEquals(BAD_REQUEST, result.status());
    assertNull(result.session().get("auth"));
  }
}
