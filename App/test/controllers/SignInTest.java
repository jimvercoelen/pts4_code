package controllers;

import com.avaje.ebean.Ebean;
import models.User;
import org.junit.Before;
import org.junit.Test;
import play.mvc.Result;
import play.test.WithApplication;

import java.util.HashMap;
import java.util.Map;

import static org.asynchttpclient.util.HttpConstants.Methods.GET;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.*;

public class SignInTest extends WithApplication {

  @Before
  public void setUp() {
    start(fakeApplication(inMemoryDatabase()));
    Ebean.save(new User("bob@example.com", "secret", "customer"));
  }

  /**
   * Testing behaviour of action {@link SignIn#getSignIn()}.
   *
   * Result status is expected to be {@code OK} (200).
   */
  @Test
  public void testGetSignIn() {
    Result result = route(fakeRequest(GET, "/signin"));

    assertEquals(OK, result.status());
    assertEquals("text/html", result.contentType().get());
    assertEquals("utf-8", result.charset().get());
  }

  /**
   * Testing behaviour of action {@link SignIn#postSignIn()}
   * with valid {@code body} values (login credentials).
   *
   * Result status is expected to be {@code SEE_OTHER} (303).
   * Session is expected to be set.
   */
  @Test
  public void testPostSignValid() {
    Map<String, String> body = new HashMap<>();
    body.put("email", "bob@example.com");
    body.put("password", "secret");

    Result result = route(fakeRequest(POST, "/signin").bodyForm(body));

    assertEquals(SEE_OTHER, result.status());
    assertEquals("bob@example.com", result.session().get("auth"));
  }

  /**
   * Testing behaviour of action {@link SignIn#postSignIn()}
   * with invalid {@code body} values (login credentials).
   *
   * Result status is expected to be {@code BAD_REQUEST} (400).
   * There is no session expected as well.
   */
  @Test
  public void testPostSignInvalid() {
    Map<String, String> body = new HashMap<>();
    body.put("email", "bob@example.com");
    body.put("password", "invalid");

    Result result = route(fakeRequest(POST, "/signin").bodyForm(body));

    assertEquals(BAD_REQUEST, result.status());
    assertNull(result.session().get("auth"));
  }
}
