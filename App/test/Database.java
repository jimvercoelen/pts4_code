import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import play.db.Databases;
import play.db.evolutions.Evolutions;
import play.test.WithApplication;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.assertTrue;


public class Database extends WithApplication {

  private play.db.Database database;

  @Before
  public void setUp() {
    database = Databases.inMemory("mydatabase",ImmutableMap.of("MODE", "MYSQL"),ImmutableMap.of("logStatements", true));
    Evolutions.applyEvolutions(database);
  }

  @Test
  public void testShutdownDatabase() {
    Evolutions.cleanupEvolutions(database);
    database.shutdown();
  }

  @Test
  public void fullTest() {
    Connection con = database.getConnection();
    try {
      con.prepareStatement("insert into User values('Bas@mail.com', 'geheim', 'customer')").execute();
      assertTrue(con.prepareStatement("select * from User where email = 'Bas@mail.com'").executeQuery().next());
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}
