package models;

import com.avaje.ebean.Ebean;
import helpers.FileSystemHelper;
import org.junit.Before;
import org.junit.Test;
import play.test.WithApplication;
import util.Constants;

import java.io.File;

import static play.test.Helpers.*;

public class FolderTest extends WithApplication {

  @Before
  public void setUp() {
    start(fakeApplication(inMemoryDatabase()));
    Ebean.save(new User("bob@example.com", "secret", "customer"));
    Ebean.save(new Folder(1, "bob@example.com", "2016"));
    Ebean.save(new FolderAccess(1, "bob@example.com"));
  }

  /*@Test
  public void fullTest() {
    assertNotNull(Folder.find.where().like("name", "Some pics").findUnique());
    assertNotNull(FolderAccess.find.where().like("folder_id", "1").like("user_email", "bob@example.com").findUnique());
  }*/

  @Test
  public void rootRemoveTest() throws Exception {
    User user = new User("bob@example.com", "secret", "Photographer");
    FileSystemHelper.createFolder(Constants.PATH_APP_ROOT_FOLDER, user.email);
//    assertEquals(true, FileSystemHelper.deleteFolder(Constants.PATH_APP_ROOT_FOLDER, user.email));
  }

  @Test
  public void rootCreationTest() throws Exception {
    User user = new User("bob@example.com", "secret", "Photographer");
//    assertEquals(true, FileSystemHelper.createFolder(Constants.PATH_APP_ROOT_FOLDER, user.email));
  }

  @Test
  public void folderCreationTest() throws Exception {
    User user = new User("bob@example.com", "secret", "Photographer");
    FileSystemHelper.createFolder(Constants.PATH_APP_ROOT_FOLDER, user.email);
    Folder folder = new Folder(1, "bob@example.com", "2k16");
    Folder folder2 = new Folder(2, "bob@example.com", "2k16");
//    assertEquals(true, folder.createFolder(user));
//    assertEquals(true, folder2.createFolder(user));
  }

  @Test
  public void createPhotoTest() throws Exception {
    File file = new File("avatar.jpg");
    User user = new User("bob@example.com", "secret", "Photographer");
    FileSystemHelper.createFolder(Constants.PATH_APP_ROOT_FOLDER, user.email);
    Folder folder = new Folder(1, "bob@example.com", "2k16");
//    folder.createFolder(user);
//    try {
//      assertEquals(true, photo.createPhoto(user, folder));
//    } catch (IOException e) {
//      e.printStackTrace();
//    }
  }

  @Test
  public void removePhotoTest() throws Exception {
    File file = new File("avatar.jpg");
    User user = new User("bob@example.com", "secret", "Photographer");
    FileSystemHelper.createFolder(Constants.PATH_APP_ROOT_FOLDER, user.email);
    Folder folder = new Folder(1, "bob@example.com", "2k16");
//    folder.createFolder(user);
//    try {
//      photo.createPhoto(user, folder);
//    } catch (IOException e) {
//      e.printStackTrace();
//    }
//    assertEquals(true, photo.deletePhoto(folder, user));
  }
}

