package models;

import com.avaje.ebean.Ebean;
import org.junit.Before;
import org.junit.Test;
import play.test.WithApplication;

import static org.junit.Assert.*;
import static play.test.Helpers.*;

public class UserTest extends WithApplication {

  @Before
  public void setUp() {
    start(fakeApplication(inMemoryDatabase()));
    Ebean.save(new User("bob@example.com", "secret", "admin"));
    Ebean.save(new User("jane@example.com", "secret", "customer"));
    Ebean.save(new User("jeff@example.com", "secret", "photographer"));
  }

  /**
   * Testing behaviour of model {@link User}.
   *
   * This 'full-test' performs searching with the expected outcomes: FOUND and NOT FOUND.
   */
  @Test
  public void fullTest() {
    // Counting records
    assertEquals(3, User.find.findRowCount());

    // Try to authenticate as users
    assertNotNull(User.find.where().like("email","bob@example.com").like("password", "secret").findUnique());
    assertNotNull(User.find.where().like("email","jane@example.com").like("password", "secret").findUnique());
    assertNull(User.find.where().like("email","jeff@example.com").like("password", "invalid").findUnique());
    assertNull(User.find.where().like("email","bob@example.com").like("password", "invalid").findUnique());
  }
}
