var combiner = require('stream-combiner2');
var gutil = require('gulp-util');

function getCombinerPipe (tasks) {
  var combined = combiner.obj(tasks);

  combined.on('error', function (error) {
    gutil.log(gutil.colors.red('Error'), error.message);
    this.emit('end');
  });

  return combined;
}

module.exports = {
  getCombinerPipe: getCombinerPipe
};
