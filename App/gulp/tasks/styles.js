var gulp = require('gulp');
var sass = require('gulp-sass');
var sassGlob = require('gulp-sass-glob');

var combiner = require('../common');

gulp.task('styles', function () {
  return combiner.getCombinerPipe([
    gulp.src([
      'public/stylesheets/main.scss'
    ]),
    sassGlob(),
    sass(),
    gulp.dest('public/stylesheets')
  ]);
});
