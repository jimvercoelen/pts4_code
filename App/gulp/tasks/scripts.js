var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

var combiner = require('../common');

gulp.task('scripts', function () {
  return combiner.getCombinerPipe([
    gulp.src([
      'public/javascripts/**/*.js'
    ]),
    concat('app.min.js'),
    uglify(),
    gulp.dest('public/javascripts')
  ]);
});
