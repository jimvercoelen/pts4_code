$(document).ready(function () {
  var $photoEditor = $('[data-remodal-id=remodal-photo-editor]');
  var $generateCode = $('[data-remodal-id=remodal-folder-code]');
  var $shoppingCart = $('[data-remodal-id=remodal-shopping-cart]');
  var $shoppingCartEdit = $shoppingCart.find('.form').find('.shopping-cart__edit');


  $generateCode.on('opening', function (event) {
    var $accessCodeElement = $('#generated-code');
    var folderId = $generateCode.attr('folder-id');
    var errorMessage = $generateCode.attr('error');
    var newAccessCode = generateCode();

    function generateCode() {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for( var i=0; i < 10; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    $.ajax({
        type: 'GET',
        url: 'http://localhost:9000/access-code/' + folderId + '/' + newAccessCode
    }).done(function () {
        $accessCodeElement.text(newAccessCode);
    }).fail(function (error) {
        console.log('Error: ', error);
        showSnackbar(errorMessage);
    });
  });

  $generateCode.on('closed', function (event) {
    location.reload();
  });

  $photoEditor.on('closing', function (event) {
    $shoppingCart.remodal().open();
  });

  $shoppingCartEdit.on('click', function (event) {
    initializePhotoEditor(imageUrl);
    $photoEditor.remodal().open();
  });

  $shoppingCart.on('closed', function (event) {
    $('.selection').slice(1).remove();
    $shoppingCart.find('.shopping-cart__price').text('');
    $shoppingCart.find('.shopping-cart__price').hide('');
    $shoppingCart.find('.shopping-cart__currency').hide('');
    $('#selection-product-type').find('.selection__control').text('');
    $('#selection-product-type')
      .removeClass('selection--floating')
      .removeClass('selection--focus')
      .removeClass('selection--show-options');
    clearShoppingCart();
  });
});
