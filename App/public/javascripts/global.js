function shakeElement($element) {
  $element
    .transition({ x: -10 }, 10)
    .transition({ x: 10 }, 10)
    .transition({ x: -6 }, 30)
    .transition({ x: 6 }, 30)
    .transition({ x: -2 }, 50)
    .transition({ x: 2 }, 50)
    .transition({ x: -1 }, 60)
    .transition({ x: 1 }, 60);
}
