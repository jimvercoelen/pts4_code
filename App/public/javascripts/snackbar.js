function showSnackbar(text) {
  var snackbar = document.getElementById('snackbar');
  var snackbarInner = document.getElementById('snackbar__inner');
  snackbarInner.innerHTML = text;
  snackbarInner.className = 'in';

  setTimeout(function() {
    snackbarInner.className = '';
  }, 4000);
}
