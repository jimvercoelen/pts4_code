$(document).ready(function () {

  initializeLocalStorage();

  $('.input').inputAnimation();
  // $('.checkbox').checkboxAnimation();
  $('#menu-trigger').menuHandler();
  $('#backdrop').menuHandler();
  $('.menu__nav--collapse').menuCollapse();
  $('.dropdown').dropdownHandler();
  $('.input--hidden-control').hiddenInputHandler();
  $('.input__control--currency').inputCurrencyHandler();
  $('.stepper').stepperify();
  $('.selection').selectionAnimation();

  // // Sign in required field (access code OR email and password)
  // var $signInInputs = $( 'input[id=sign-in-access-code],input[id=sign-in-email],input[id=sign-in-password');
  // $signInInputs.on('input', function () {
  //   $signInInputs.not( this ).prop( 'required', !$( this ).val().length );
  // });
});

function initializeLocalStorage () {
  var cart = JSON.parse(localStorage.getItem('cart'));
  if (cart == null) {
    cart = {
      products: [],
      totalPrice: 0
    };
  }

  localStorage.setItem('cart', JSON.stringify(cart));
}
