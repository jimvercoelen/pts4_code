$(document).ready(function () {
  Waves.init();
  Waves.attach('.menu__nav > li > a', ['waves-attach', 'waves-effect']);
  Waves.attach('.header__dropdown__content > ul > li > a', ['waves-attach', 'waves-effect']);
  Waves.attach('.button', ['waves-attach', 'waves-effect']);
  Waves.attach('.selection__option', ['waves-attach', 'waves-effect']);
  Waves.attach('.waves-icon', ['waves-circle']);
  // Waves.attach('.stepper--control', ['waves-attach', 'waves-effect']);
});
