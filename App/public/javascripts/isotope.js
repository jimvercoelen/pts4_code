$(document).ready(function () {
  var $album = $('.grid__album');
  var $folderGrid = $('.grid__folders');

  $album.imagesLoaded(function () {
    $album.isotope({
      itemSelector: '.photo',
      layoutMode: 'masonry',
      masonry: {
        columnWidth: $album.width() / 12
      }
    });
  });

  $folderGrid.isotope({
    itemSelector: '.folder',
    layoutMode: 'masonry',
    masonry: {
      columnWidth: $album.width() / 8,
      gutter: 20
    }
  });
});
