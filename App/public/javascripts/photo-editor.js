var photoEditorSDK = null;
var imageUrl;

function initializePhotoEditor(photo) {
  var $photoEditor = $('[data-remodal-id=remodal-photo-editor]');
  var initImage = new Image();
  var folderId = $('.grid__album').attr('folder-id');
  var editor = document.getElementById('photo-editor');

  initImage.addEventListener('load', function () {
    if (photoEditorSDK !== null) {
      photoEditorSDK.setImage(initImage);
      return;
    }

    photoEditorSDK = new PhotoEditorSDK.UI.ReactUI({
      container: editor,
      title: '',
      enableUpload: false,
      enableWebcam: false,
      editor: {
        image: initImage,
        export: {
          // showButton: false
          download: false,
          type: PhotoEditorSDK.RenderType.DATAURL
        }
      },
      assets: {
        baseUrl: '../assets/assets'
      }
    });

    photoEditorSDK.on('export', function (dataUrl) {
      console.log('saving');

      $.ajax({
        type: 'POST',
        url: 'http://localhost:9000/folder/' + folderId + '/photo/edit',
        data: {
          image: dataUrl
        }
      }).done(function () {
        console.log('saved');

        // Show notification and close editor
        // TODO: language
        showSnackbar('Opgeslagen');
        $photoEditor.remodal().close();
      });
    });
  });

  initImage.src = photo;
}
