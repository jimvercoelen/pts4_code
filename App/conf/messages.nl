appTitle=Fotowinkel

gallery=Gallerij
settings=Instellingen
language=Taal
language.nl=Nederlands
language.en=Engels

signIn=Inloggen
signUp=Registreren
signOut=Uitloggen
addAlbum=Album toevoegen
deleteAlbum=Album verwijderen
viewAlbums=Bekijk albums
noAlbums=U heeft nog geen albums
share=Delen
shareAlbum=Album delen
shareAlbum.email.unknown=Onbekend emailadres
shareAlbum.email.exists=Album gedeeld
shareAlbum.email.success=Album gedeeld
shareAlbum.delete.share=Gebruiker verwijderd
sharedAlbum=Bekijk gedeeld
owner=Eigenaar
addPhoto=Foto toevoegen
addToCart=Toevoegen aan winkelwagen
pricePerPhoto=Prijs per foto
back=Terug
#edit=Wijzigen
edit=Bewerken
save=Opslaan
saved=Opgeslagen
add=Toevoegen
albumName=Albumnaam
albumSettings= Albuminstellingen
albumNew=Nieuw album aangemaakt
newAlbumTitle=Nieuw album
albumRemoved=Album verwijderd
photoAdded=Foto's toegevoegd
photoRemoved=Foto verwijderd
invite=Uitnodigen

email=Email
password=Wachtwoord
remember=Onthoud mij
forgotten=Wachtwoord vergeten
click=Klik
here=hier
clickLink= om uw wachtwoord opnieuw in te stellen
mailSubject=Herstel je wachtwoord
iamnew=Ik ben nieuw!
iam=Ik ben
photographer=Fotograaf
customer=Klant

incorrect.email=Het emailadres of
incorrect.password=het wachtwoord is onjuist

password.forgotten=Wachtwoord opnieuw aanvragen
password.forgotten.submit=Aanvraag versturen
password.forgotten.notification=Aanvraag is verstuurd
password.recovery.submit=Wachtwoord herstellen
password.recovery.notification=Wachtwoord is hersteld
password.recovery.expired=Aanvraag is verlopen

email.unknown=E-mailadres is onbekend

error.oops=Oeps, er ging iets fout!
error.duplicated.entry=Dit e-mailadres is al in gebruik
error.load=Fout met laden


unknownEmail=Onbekend emailadres

folderTitle=Maptitel
folderSettings=Album instelligen
newFolder=Nieuwe map aangemaakt
deleteFolder=Verwijder map
removedFolder=Map verwijderd
shareFolder=Map delen
sharedFolder=Map gedeeld
sharedWith=Gedeeld met
unSharedFolder=Map niet langer gedeeld met deze persoon

productChoice=Productkeuze

shoppingCart=Winkelwagen
addedToCart=Toegevoegd aan winkelwagen
checkProducts=Producten controleren
next=Volgende
details=Gegevens
checkout=Afrekenen
city=Woonplaats
address=Adres
address.title=Vul alstublieft uw adres in \ne.g. Marktstraat 21
zipcode=Postcode
zipcode.title=Vul alstublieft uw postcode in \ne.g. 1234AB
phone=Telefoon
orderSend=Bedankt voor uw bestelling
order=Bestelling
orderDetails=Besteldetails
totalPrice=Totaalprijs
orderMade=Bestelling geplaatst
mailDescription=Bedankt voor uw bestelling. Hieronder staat een overzicht van uw bestelde producten

or=Of
code=Code
enterYourCode=Vul hier uw toegangscode in
enterYourDetails=Vul uw gegevens in
generateCode=Genereer toegangscode
generate=Genereer
accessCode=Toegangscode
accessCode.delete=Toegangscode is verwijderd
accessCode.unknown=Of onbekende toegangscode

products=Producten
products.check=Producten bekijken
product.edit=Product wijzigen
product.remove=Product verwijderen

photos=Foto''s
photos.check=Foto''s bekijken
photo.remove=Foto verwijderen

orders=Bestellingen
processed=Verwerkt
inStock=op voorraad
