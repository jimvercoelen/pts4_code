# Fotowinkel schema

# --- !Ups

CREATE TABLE Product (
  id                  BIGINT(200) NOT NULL AUTO_INCREMENT,
  `type`              VARCHAR(20) NOT NULL,
  option_name         VARCHAR(20) NOT NULL,
  option_value        VARCHAR(20) NOT NULL,
  price               DOUBLE(20,2) DEFAULT 0,
  stock               INT(20) DEFAULT 0,
  code                VARCHAR(20) NOT NULL,
  `language`          VARCHAR(10) DEFAULT 'NL',


  PRIMARY KEY (id, `type`)

);

# --- !Downs

DROP TABLE Product;
