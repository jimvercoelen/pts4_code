# Fotowinkel schema

# --- !Ups

CREATE TABLE Password_recovery (
  id            BIGINT(200) NOT NULL AUTO_INCREMENT,
  email         VARCHAR(255) NOT NULL,
  token         VARCHAR(255) NOT NULL,

  PRIMARY KEY (id)
);

# --- !Downs

DROP TABLE Password_recovery;
