# Fotowinkel schema

# --- !Ups

CREATE TABLE Access_code (
  id               BIGINT(200) NOT NULL AUTO_INCREMENT,
  folder_id        BIGINT(200) NOT NULL,
  access_code       VARCHAR(20) NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (folder_id) REFERENCES Play.Folder (id)
);

# --- !Downs

DROP TABLE Access_code;
