# Fotowinkel schema

# --- !Ups

CREATE TABLE Folder_access (
  id                BIGINT(200) NOT NULL AUTO_INCREMENT,
  folder_id         BIGINT(200) NOT NULL,
  user_email        varchar(255) NOT NULL,

  PRIMARY KEY (id),
  UNIQUE (folder_id, user_email),
  FOREIGN KEY (folder_id) REFERENCES Folder (id),
  FOREIGN KEY (user_email) REFERENCES Play.User (email)
);

# --- !Downs

DROP TABLE Folder_access;
