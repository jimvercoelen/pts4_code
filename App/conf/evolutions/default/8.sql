# Fotowinkel schema

# --- !Ups

CREATE TABLE Orderline (
  id            BIGINT(200) NOT NULL AUTO_INCREMENT,
  orderid       BIGINT(200) NOT NULL,
  photourl      VARCHAR(200) NOT NULL,
  options       VARCHAR(200) NOT NULL,
  price         DOUBLE NOT NULL,
  product       VARCHAR(20) NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (orderid) REFERENCES Play.Orderr (id)
);

# --- !Downs

DROP TABLE Orderline;
