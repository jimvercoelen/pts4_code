# Fotowinkel schema

# --- !Ups

CREATE TABLE Folder (
  id            BIGINT(200) NOT NULL AUTO_INCREMENT,
  creator       varchar(255) NOT NULL,
  name          VARCHAR(255) NOT NULL,
  price         DECIMAL(5,2),

  PRIMARY KEY (id),
  FOREIGN KEY (creator) REFERENCES Play.User (email)
);

# --- !Downs

DROP TABLE Folder;
