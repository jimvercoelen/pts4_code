# Fotowinkel schema

# --- !Ups

CREATE TABLE Folder_product_type (
  id            BIGINT(200) NOT NULL AUTO_INCREMENT,
  folder_id     BIGINT(200) NOT NULL,
  product_type  VARCHAR(20) NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (folder_id) REFERENCES Play.Folder (id)
);

# --- !Downs

DROP TABLE Folder_product_type;
