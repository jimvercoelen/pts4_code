# Fotowinkel schema

# --- !Ups

CREATE TABLE Orderr (
  id            BIGINT(200) NOT NULL AUTO_INCREMENT,
  customer      VARCHAR(100) NOT NULL,
  totalprice    DOUBLE NOT NULL,
  address       VARCHAR(200) NOT NULL,
  city          VARCHAR(200) NOT NULL,
  zipcode       VARCHAR(50) NOT NULL,
  phonenumber   VARCHAR(50) NOT NULL,
  processed     INT(1) DEFAULT 0,

  PRIMARY KEY (id)
);

# --- !Downs

DROP TABLE Orderr;
