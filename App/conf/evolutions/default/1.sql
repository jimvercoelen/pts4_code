# Fotowinkel schema

# --- !Ups

CREATE TABLE User (
  email     VARCHAR(255) UNIQUE NOT NULL,
  password  VARCHAR(255) NOT NULL,
  user_type  VARCHAR(255) NOT NULL,

  PRIMARY KEY (email)
);

# --- !Downs

DROP TABLE User;

-- INSERT INTO User VALUES ('admin@mail.com', '123456', 'admin');
